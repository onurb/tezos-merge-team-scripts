This script generates an overview of open MRs, their WIP state, from
whom action is required, number of open threads etc. Output is in HTML
format. The output is published regularly (every ~20 minutes) at:

    https://arvidnl.gitlab.io/tezos-merge-team-scripts/


# Setup

Install dependencies with poetry:

    poetry install

Get a GitLab access token:

https://gitlab.com/-/profile/personal_access_tokens

Only the scope `read_api` is necessary.

This token will be passed to the script as a command-line parameter.

# Usage

To build the overview locally, run

    poetry run python3 main.py --private-token=<YOUR_TOKEN>
