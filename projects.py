from projects_spec import *

DASHBOARD_MAINTAINERS = Users('mbouaziz')

PROJECTS = List(
    Group(
        'tezos',
        dispatchers=Issue('tezos/tezos#1062'),
        issuewatchers=Issue('tezos/tezos#1061'),
        pre_weight=8,
    ),
    Project('tezos/opam-repository', dispatchers=Users('pirbo'), pre_weight=8),
    Project('smondet/merbocop', pre_weight=7),
    Project('arvidnl/tezos-merge-team-scripts', pre_weight=6),
    Project('metastatedev/tezos', pre_weight=5),
    Group('nomadic-labs'),
    Group('tzip'),
)
