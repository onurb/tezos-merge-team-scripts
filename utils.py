class Wrapper:
    def __init__(self, wrapped):
        self.wrapped = wrapped

    def get_wrapped_attr(self, attr):
        try:
            return getattr(self.wrapped, attr)
        except AttributeError as error:
            if attr == 'wrapped_more':
                raise error
            more = getattr(self, 'wrapped_more', None)
            if more is None:
                raise error
            else:
                self.wrapped = more
                return getattr(more, attr)

    def __getattr__(self, attr):
        return self.get_wrapped_attr(attr)
